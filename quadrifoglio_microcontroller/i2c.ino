//// callback for received data //Slave mode I2C
//void receiveHandler(int byteCount) { //This function is entered even on a request to send, as the smbus command arrives first:
//  byte which = Wire.peek();   //Check what the request is. If < 127, this is a  request to send, so nothing should be done here
//  
//  if(which>=127){ //If command is >= 127, this is incoming data. Else it's a request to transmit, then nothing should be done here
//    Wire.read(); //Get first item from receive buffer, this is the command. GetAnything assumes cache only contains message
//    if(which == 127){ GetAnything(Command);}   //Actual read is done here to get the command out from the buffer
//    else if(which == 128){; GetAnything(Command);}
//    else{
//      while(Wire.available()){Wire.read();} //Flush cache in case command was corrupted (so received data does not stay in it)
//      //SendAnything(errorMsg); 
//    }
//  }
//}

// callback for sending data
//void requestHandler() {
//  byte which = Wire.read();
//  if(which == 2) {SendAnything(Feedback);}
//  else if(which == 3) {SendAnything(Feedback);}
//  else if(which == 1) SendAnything(Feedback);
//  else{
//    //SendAnything(errorMsg);
//    //while(Wire.available()){Wire.read();}
//  }
//  commsDone = true; //Mark that data is sent, this causes struct refresh
//}

#ifdef I2C_ON
template <class T> int GetAnything(int address, const T& value){
    byte* p = (byte*)(void*)&value;
    unsigned int i;
    Wire.requestFrom(address,sizeof(value));
    for (i = 0; i < sizeof(value); i++){ //Read until struct is full
      if(Wire.available()) *p++ = Wire.read(); //Read if there is stuff incoming, else abort
      else return -1;
    }
    return i;
}

template <class T> int SendAnything(int address, const T& value){
    const byte* p = (const byte*)(const void*)&value;
    unsigned int i;
    Wire.beginTransmission(address);
    for (i = 0; i < sizeof(value); i++){
      const byte b = *p;
      Wire.write(b), p++;
    }
    Wire.endTransmission();
    return i;
}


uint8_t LedTester(uint8_t mode){
  static unsigned long asd = 0;
  if(millis()-asd > 30){
    asd = millis();
      switch(mode){
        case LED_OFF:
          return 0;
          break;
        case LED_SPEED:
          return 0;
          break;
        case LED_CURRENT_SERVO:
          return (uint8_t)constrain(map(servoCurrent.filteredValue(),0,1500,0,255),0,255);
          break;
        case LED_CURRENT_MOTOR:
          return (uint8_t)constrain(map(motorCurrent.filteredValue(),0,4000,0,255),0,255);
          break;
        case LED_BATTERY:
          return (uint8_t)constrain(map((unsigned int)batteryVoltage.filteredValue(),BATT_CUTOFF_MV,BATT_FULL_MV,0,255),0,255);
          break;
        default:
          return 0;
          break;
  }
    
  }
}
#endif
