
#include <Arduino.h>
#include "quadriPID.h"
#define ACTIVE 1                  //Pid status, active means output is controlled, inactive that it isn't.
#define INACTIVE 0


quadriPID::quadriPID(float p, float i, float d, float lolim, float hilim) {
  _ITerm = _error = _dInput = 0.0;
  _kp = p;
  _ki = i;
  _kd = d;
  output = input = 0.0;
  _lolim = lolim;
  _hilim = hilim;
  _lastInput = 0.0;
  _lastEncoderPos = 0.0;
  _lastNonZeroInputTime = millis();
  //_pidActive = true;
}

float quadriPID::Compute(float setpoint, float currentEncoderPos) {
  //if(!_pidActive) return 0;
  unsigned long timeNow = millis();
  if(abs(setpoint) > 1.5) _lastNonZeroInputTime = timeNow;

  input = currentEncoderPos - _lastEncoderPos;
  _lastEncoderPos = currentEncoderPos;

  /*Compute all the working error variables*/
  float _error = setpoint - input;
  _ITerm += _ki * _error;

  if (_ITerm > _hilim) _ITerm = _hilim;                     //If integrator exeeds limits, clamp it.
  else if (_ITerm < _lolim) _ITerm = _lolim;               //Same for the other direction.

  float _dInput = input - _lastInput;          //Derivative of input instead of error to get rid of spiking

  //Do some special cases
  if (timeNow - _lastNonZeroInputTime > 250 && abs(_error) < 2.0) {
    _ITerm = 0.0;
  }

  /*Compute PID Output*/
  output = (_kp * _error + _ITerm - _kd * _dInput);            //Flip it to get right direction
  if (output > _hilim) output = _hilim;                   //Same clamping as before, now for whole output.
  else if (output < _lolim) output = _lolim;                //If implementing onthefly changing remember to do this at change too.

  /*Remember some variables for next time*/
  _lastInput = input;
  return output;


}
void quadriPID::Reset(float currentEncoderPos) {
  _ITerm = 0.0;
  _lastEncoderPos = currentEncoderPos; //Keep this current so error signal goes to zero
}
//~
//~ void fixPID::PidInitialize(){								//Resets PID error terms, inhibits jumping when restarting PID. Do not use directly, "PidActive" function takes care of this.
//~ _lastInput = float(*_input);
//~ _ITerm = output;
//~ if(_ITerm>90.0) _ITerm = 90.0;
//~ else if(_ITerm < -90.0) _ITerm = -90.0;
//~
//~ }
//~
//~
//~ void fixPID::PidActivate(short mode){					//Turns a pid on or off and re-initializes it when turning on.
//~
//~ _newState = (mode == ACTIVE);
//~ if(_newState && !_pidActive){
//~ PidInitialize();
//~ }
//~ _pidActive = _newState;
//~ }
