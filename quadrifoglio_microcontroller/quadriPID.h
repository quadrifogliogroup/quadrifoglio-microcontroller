#include <Arduino.h>

#ifndef quadriPID_h
#define quadriPID_h

class quadriPID{
  public:
    quadriPID(float p, float i, float d, float lolim, float hilim);
    float Compute(float setpoint, float currentEncoderPos);
    void Reset(float currentEncoderPos);
    //void PidActivate(short mode);
    //void PidInitialize();
    float _ITerm, _error, _dInput;		//Error is not static as it's computed every time again
    float _kp;
    float _ki;
    float _kd;
    float output;
    float input;
  private:
	//short _pidActive;
	//short _newState;
	float _hilim;
	float _lolim;
  float _lastEncoderPos;
	
	//float _error;
  float _lastInput;				//This is needed to derivate input directly
  unsigned long _lastNonZeroInputTime;
    
};

#endif
