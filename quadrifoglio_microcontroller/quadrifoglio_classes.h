#include <Arduino.h>


//==========StoreFilter===============
#ifndef StoreFilter_h
#define StoreFilter_h

class StoreFilter{
  public:
    StoreFilter(uint8_t, unsigned int, float);
    float filteredValue();
    void runEMA();
    uint8_t pin();
    uint16_t value;
  private:
    uint8_t _pin;
    float _alpha;
    float _filtered;
};
#endif

#ifndef QuadriServo_h
#define QuadriServo_h

class QuadriServo{
  public:
    QuadriServo(unsigned int lowPulse, unsigned int midPulse, unsigned int highPulse, float lowAngle, float highAngle);
    uint16_t Degrees(float degIn); //Maps degrees to servo pulse. Lower values turn wheel counterclockwise, servo turns clockwise when viewed from above.
    uint16_t Range1024(int input);  //Maps an integer in range -1024 / 1024 to the maximum and minimum of servo travel, for testing
    uint16_t LowPulse();
    uint16_t MidPulse();
    uint16_t HighPulse();
    float CommandedAngle();

  private:
    uint16_t _lowPulse;  //Minimum pulse the servo accepts
    uint16_t _midPulse;   //Pulse at which the servo is centered as installed
    uint16_t _highPulse; //Maximum pulse the servo accepts
    float _lowAngle; //Angle at lowest pulse, used to interpolate servo position between center and maximums
    float _highAngle; //Angle at highest pulse, used to interpolate servo positions
    float _commandedAngle;  //Current commanded angle
    float* _angleADC;  //Pointer to information about servo angle
};

#endif
