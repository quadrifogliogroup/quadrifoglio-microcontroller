#define ACTUATORS_ON //If this is defined, actuators are enabled, robot can move. Else it is completely immobilized for testing
//#define CUTOFF_OVERRIDE  //Disables low voltage cutoff.
#define ROS_MESSAGING_ON    //If this is defined, ros messages are published and subscribed
//#define DEBUGSERIAL_ON
//#define I2C_ON  //i2c functionality, causes the occasional hang due to timeout + watchdog?
#define IMU_ON


#ifdef ROS_MESSAGING_ON
#include <ros.h>

//publish
#include <quadrifoglio_msgs/encoders.h>
#include <quadrifoglio_msgs/electrics.h>
#include <quadrifoglio_msgs/ultrasound.h>
#include <quadrifoglio_msgs/imuLean.h>
#include <quadrifoglio_msgs/rawJoints.h>
#include <quadrifoglio_msgs/rawJoy.h>

//Subscribe
#include <quadrifoglio_msgs/baseDriveCmd.h>
#include <quadrifoglio_msgs/ledstrips.h>


//==================== JOY CONFIGURATION TARANIS ======================
/*
  input         - radio ch  - msg mapping

  throttle/pot  - channel 1 - ax1
  roll/pot      - channel 2 - ax2
  pitch/pot     - channel 3 - ax3
  yaw/pot       - channel 4 - ax4
  wiperleft/pot - channel 5 - ax5
  wiperright/pot- channel 6 -
  s1/pot        - channel 7 - ax6
  sa/3way       - channel 8 - 0-1
  sb/3way       - channel 9 - 2-3
  se/3way       - channel 10 - 4-5
  sg/3way       - channel 11 - 6-7

*/

#endif

#include <avr/wdt.h>

#ifdef I2C_ON
//#include <Transcutale_I2C_Slave.h>
#include "messages_quadri.h"
#include <Wire.h>
#endif

#ifdef IMU_ON
#include <Wire.h>
#include "MPU9250.h"
MPU9250 mpu9250;
#endif

#include "SBUS.h"
#include "tweeter.h"
#include <toneAC.h>

#include "quadrifoglio_classes.h"
#include "quadriPID.h"


SBUS sbus(Serial3);

#define MAIN_UC 0x03 //I2C addresses
#define FRONT_ULTRA 0x04
#define REAR_ULTRA 0x05
#define RASPI 0x07
#define SINGLE_TRANS_LIMIT 32

#define TODEG 57.29577951          //Conversion factors for degrees & radians
#define TORAD 0.017453293         //The AVR cannot reach this level of precision but w/e
const float pi = 3.14159265;

#ifdef ROS_MESSAGING_ON

class NewHardware : public ArduinoHardware  //Do this to change bauds and the port
{
  public:
    NewHardware(): ArduinoHardware(&Serial2, 250000) {};  //115200
};

ros::NodeHandle_<NewHardware,16,16,512,512>  nh;
#endif

Beeperer beeper;  //ToneAC version has fixed pins

float testfloat = 0;
unsigned int loopRate = 0;

float pitchEstimateAccel = 0;
float rollEstimateAccel = 0;

float pitchEstimateGyro = 0;
float rollEstimateGyro = 0;
float yawEstimateGyro = 0;

const float wheelbase = 25.3;  //Robot wheelbase and track, for ackerman angle calculations
const float track = 15.5;
const float inPlaceTurnAngle = TODEG * atan((wheelbase / 2) / (track / 2)); //
const float minPivotDistanceX = 10.0; //Minimum distance of ackermann convergence point from vehicle centerpoint in the x direction, values below this are clamped. Could be calculated..
boolean commsDone = false;

//Motor module numbers
//1--2
// ||
//3--4

#define BLUELED 27
//yellowled PJ7

#define BATT_DIV_R1 9100 //Resistors for battery voltage divider
#define BATT_DIV_R2 5600
const int BATT_WARN_MV = 7100; //Millivolts when battery status warning comes on
const int BATT_AUDIO_WARN_MV = 6950;  //Millivolts when battery status audio warning comes on
const int BATT_CUTOFF_MV = 6800; //Millivolts when robot shuts down due to low voltage
const int BATT_FULL_MV = 8400;
const int sys_5v = 500; //System 5 volt actual voltage (measured), with 2 decimals

#define MOT2A 8
#define MOT2B 47
#define MOT2PWM 13

#define MOT1A 42
//#define MOT1B PH7
#define MOT1PWM 4

//#define MOT3A PJ4
//#define MOT3B PJ3
#define MOT3PWM 10

#define MOT4A A9
#define MOT4B A11
#define MOT4PWM 9
//#define MOTSTBY PJ5

#define SERVO1 5
#define SERVO2 46
#define SERVO3 45
#define SERVO4 44

QuadriServo Servo1(950, 3200, 5150, -92, 85);
QuadriServo Servo2(950, 2900, 5150, -80, 95);
QuadriServo Servo3(950, 2750, 5150, -80, 100);
QuadriServo Servo4(950, 2850, 5150, -80, 93);

int throttleStick = 0;  //These are updated by the radio
int pitchStick = 0;     //Scale is -1024 | 1023
int frontWheelStick = 0;//-1024 to 1023 With 0 being center
int rearWheelStick = 0;
int leftSlider = 0;
int rightSlider = 0;
boolean radioActive = false; //If there is a valid radio signal
unsigned int radioTimeout = 500; //After this many milliseconds consider radio dead and stop
uint8_t ledController = 0; //This is led control as read from the radio.

volatile long pos1 = 0;  //Encoder position measurement (should be int..)
volatile long pos2 = 0;
volatile long pos3 = 0;
volatile long pos4 = 0;

const byte curMotADC = A0;  //Analog pin definitions
const byte curRaspADC = A1;
const byte battSensADC = A2;
const byte curServADC = A3;
const byte curUcADC = A4;
const byte aux1ADC = A5;
const byte Servo1ADC = A10;
const byte aux2ADC = A7;
const byte Servo2ADC = A6;
const byte Servo3ADC = A13;
const byte Servo4ADC = A14;


//Pin, start value, aplha. Larger alpha makes for faster response, lower for better filtering
StoreFilter motorCurrent(curMotADC, 0, 0.3); //Milliamperes
StoreFilter raspiCurrent(curRaspADC, 0, 0.3); //Milliamperes
StoreFilter batteryVoltage(battSensADC, 7400, 0.2); //Millivolts
StoreFilter servoCurrent(curServADC, 0, 0.3); //Milliamperes
StoreFilter ucCurrent(curUcADC, 0, 0.2); //Milliamperes

StoreFilter servoPos1(Servo1ADC, 0, 0.3); //Raw
StoreFilter servoPos2(Servo2ADC, 0, 0.3); //Raw
StoreFilter servoPos3(Servo3ADC, 0, 0.3); //Raw
StoreFilter servoPos4(Servo4ADC, 0, 0.3); //Raw

StoreFilter auxAnalog1(aux1ADC, 0, 0.2); //Raw
StoreFilter auxAnalog2(aux2ADC, 0, 0.2); //Raw

StoreFilter *analogArray[] = {&motorCurrent, &raspiCurrent, &batteryVoltage, &servoCurrent, &ucCurrent, &servoPos1, &servoPos2, &servoPos3, &servoPos4};  //Array to the pdated analog channels

//PID stuff

const float p = 2.5;
const float i = 0.23;
const float d = 0.0;

quadriPID mot1PID(p, i, d, -255, 255); // 1.9 0.23 0.0
quadriPID mot2PID(p, i, d, -255, 255); // 1.9 0.23 0.0
quadriPID mot3PID(p, i, d, -255, 255); // 1.9 0.23 0.0
quadriPID mot4PID(p, i, d, -255, 255); // 1.9 0.23 0.0

const unsigned long testPidRate = 20000;  //Microseconds
const float encPerMeter = 48 * 16 * 2 * (1.0 / (pi * 0.063)); //Encoder pulses per meter, | gearratio*encoder pulses*2ch quadrature*(1m/wheelcircumference) |
const float pidEncPerMs = encPerMeter * ((float)testPidRate) / 1000000.0; //How many encoder pulses will happen per pid delta t per m/s

//struct SonarStruct {
//  uint16_t frontLeftCm = 0;
//  uint32_t frontLeftMillis = 0;
//  uint16_t frontRightCm = 0;
//  uint32_t frontRightMillis = 0;
//  uint16_t frontCenterCm = 0;
//  uint32_t frontCenterMillis = 0;
//  uint16_t rearLeftCm = 0;
//  uint32_t rearLeftMillis = 0;
//  uint16_t rearRightCm = 0;
//  uint32_t rearRightMillis = 0;
//  uint16_t rearCenterCm = 0;
//  uint32_t rearCenterMillis = 0;
//};

struct BaseDriveCommand {
  float frontLeftServo;   //Servo angles, in degrees
  float frontRightServo;
  float rearLeftServo;
  float rearRightServo;
  float frontLeftMot;    //Motor target velocities in m/s
  float frontRightMot;
  float rearLeftMot;
  float rearRightMot;
};

bool newSatelliteData = false;  //This is set true when new comms from satellites arrive, which causes the corresponding ros message to be published

#ifdef I2C_ON

LedMessage ledMsg = {0, 0, 0, 3000};
SonarMessage sonarMsgFront = {0, 0, 0, 0};  //i2c messages to which sonar stuff is read
SonarMessage sonarMsgRear = {0, 0, 0, 0};
//SonarStruct sonarStruct = {}; //All to zero


FeedbackMsg Feedback = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};


#define LED_OFF 0  //Possible states for ledstrips driven by satellite uCs
#define LED_SPEED 1
#define LED_CURRENT_SERVO 2
#define LED_CURRENT_MOTOR 3
#define LED_BATTERY 4

#endif

BaseDriveCommand Command = {0, 0, 0, 0, 0, 0, 0, 0};
boolean baseDriveCmdReceived = false; //Flag for deadmanswitch, no messages cut off motors

#ifdef ROS_MESSAGING_ON

quadrifoglio_msgs::encoders encoder_msg;
ros::Publisher encoderPub("quadrifoglio/encoders", &encoder_msg);

quadrifoglio_msgs::electrics electrics_msg;
ros::Publisher electricsPub("quadrifoglio/electrics", &electrics_msg);

quadrifoglio_msgs::ultrasound ultrasound_msg;
ros::Publisher ultrasoundPub("quadrifoglio/ultrasound", &ultrasound_msg);

quadrifoglio_msgs::rawJoints joint_msg;
ros::Publisher jointPub("quadrifoglio/rawJoints", &joint_msg);

quadrifoglio_msgs::imuLean imu_msg;
ros::Publisher imuPub("quadrifoglio/imu_lean", &imu_msg);

quadrifoglio_msgs::rawJoy joy_msg;
ros::Publisher joyPub("quadrifoglio/rawJoy", &joy_msg);

void baseDriveCb( const quadrifoglio_msgs::baseDriveCmd& base_drive_msg) {  //Copy these for later use
  Command.frontLeftServo = base_drive_msg.frontLeftServo;
  Command.frontRightServo = base_drive_msg.frontRightServo;
  Command.rearLeftServo = base_drive_msg.rearLeftServo;
  Command.rearRightServo = base_drive_msg.rearRightServo;

  Command.frontLeftMot = base_drive_msg.frontLeftMot;   //Motor command is +- 255 with 255 offset, so 0-510
  Command.frontRightMot = base_drive_msg.frontRightMot;
  Command.rearLeftMot = base_drive_msg.rearLeftMot;
  Command.rearRightMot = base_drive_msg.rearRightMot;

  baseDriveCmdReceived = true;
}

ros::Subscriber<quadrifoglio_msgs::baseDriveCmd> baseDriveSub("quadrifoglio/baseDriveCmd", &baseDriveCb );
#endif


void setup() {
  wdt_enable(WDTO_2S);  //Longer time for setup
  // Cutoff pin:
  PORTJ |= _BV(PJ2); //Pullup on cutoff pin, this first as making it an output would briefly pull it low
  DDRJ |= _BV(PJ2);  //Cutoff pin high

  //LED
  pinMode(BLUELED, OUTPUT);
  DDRJ |= 0x80; //OTherled


#ifdef ACTUATORS_ON// Motors
  pinMode(MOT2A, OUTPUT);  //Motor 2
  pinMode(MOT2B, OUTPUT);
  pinMode(MOT2PWM, OUTPUT);
  digitalWrite(MOT2A, HIGH);
  digitalWrite(MOT2B, LOW);

  pinMode(MOT1A , OUTPUT); //Motor 1
  DDRH |= 0x80; //MOT1B
  pinMode(MOT1PWM, OUTPUT);
  digitalWrite(MOT1A, LOW);
  PORTH |= _BV(PH7); //MOT1B high
  //
  DDRJ |= _BV(PJ4); //MOT3A PJ4 to output
  DDRJ |= _BV(PJ3); //MOT3B PJ3 to output
  pinMode(MOT3PWM, OUTPUT);
  PORTJ &= ~_BV(PJ4); //MOT3A low
  PORTJ |= _BV(PJ3); //MOT3B high
  //
  pinMode(MOT4A, OUTPUT); //Motor 4
  pinMode(MOT4B, OUTPUT);
  pinMode(MOT4PWM, OUTPUT);
  digitalWrite(MOT4A, HIGH);
  digitalWrite(MOT4B, LOW);


  DDRJ |= 0x20;  //Motor controller standby port J5 to output
  PORTJ |= 0x20; //Motor controller standby port J5 high


  //=============CONFIGURING SERVO SIGNALS====================
  //Timer 3 for servo 1 only, using OC3A. Rest are encoder interrupts for this timer
  pinMode(SERVO1 , OUTPUT); //Servo 2 pin
  TCCR3A = _BV(WGM31) | _BV(COM3A1);  //SET WGM's to fast PWM mode with ICR top, COM3 to non inverting mode
  TCCR3B =  _BV(WGM32) | _BV(WGM33) | _BV(CS31);  //Prescaler to 8
  ICR3 = 39999;//this is TOP, set to 39999 making a 50 hz pwm signal for standard servos
  OCR3A = Servo1.MidPulse(); //Center position for servo 1

  //Timer 5 for servos 2, 3 and 4
  pinMode(SERVO2 , OUTPUT); //Servo 2 pin
  pinMode(SERVO3 , OUTPUT); //Servo 2 pin
  pinMode(SERVO4 , OUTPUT); //Servo 2 pin
  TCCR5A = _BV(WGM51) | _BV(COM5A1) | _BV(COM5B1) | _BV(COM5C1);  //SET WGM's to fast PWM mode with ICR top, COM3 to non inverting mode
  TCCR5B =  _BV(WGM52) | _BV(WGM53) | _BV(CS51);  //Prescaler to 8
  ICR5 = 39999;//this is TOP, set to 39999 making a 50 hz pwm signal for standard servos
  OCR5A = Servo2.MidPulse(); //Center position for servo 1
  OCR5B = Servo3.MidPulse(); //Center position for servo 3
  OCR5C = Servo4.MidPulse(); //Center position for servo 4

  //TCCR2B = _BV(CS20); //Timer 2 62khz pwm

#endif  //End of actuator block

  //=================PULLUPS FOR OPEN COLLECTOR ENCODERS=======
  DDRE &= ~(_BV(PE6) | _BV(PE7)); //Encoder 1, ports as inputs (clear DDR)
  PORTE |= _BV(PE6) | _BV(PE7);   //Turn on internal pullup (set PORT)

  pinMode(18, INPUT_PULLUP); //Encoder 2
  pinMode(19, INPUT_PULLUP);

  pinMode(3, INPUT_PULLUP); //Encoder 3
  DDRJ &= ~(_BV(PJ6));  //Input
  PORTJ |= _BV(PJ6);    //Pullup

  pinMode(2, INPUT_PULLUP); //Encoder 4
  DDRK &= ~(_BV(PK7));  //Input
  PORTK |= _BV(PK7);    //Pullup

  //===============CONFIGURING INTERRUPTS FOR ENCODERS=====================
  //      |-----ENCODER2------| |-ENCODER3-| |-ENCODER4-| |------ENCODER1------|
  EIMSK = _BV(INT2) | _BV(INT3) | _BV(INT4) | _BV(INT5) | _BV(INT6) | _BV(INT7); //Enabling these interrupts
  EICRA = _BV(ISC20) | _BV(ISC30);                            //Any edge for interrupts 2 and 3. 1 and 2 are same as i2c pins and not used
  EICRB = _BV(ISC40) | _BV(ISC50) | _BV(ISC60) | _BV(ISC70);  //Any edge for interrupts 4 5 6 and 7

  PCICR = _BV(PCIE1) | _BV(PCIE2);   //Enabling pin change interrupts. 1 for encoder 4,  2 for encoder 3, 0 for gyro
  PCMSK1 = _BV(PCINT15);  //Enable pin change interrupt on encoder pin
  PCMSK2 = _BV(PCINT23);



  sbus.begin(false); //Do not use timer!

#if defined(I2C_ON) || defined(IMU_ON)
  // initialize i2c as master
  Wire.begin();
  Wire.setClock(400000);      //Fastah

#endif
#ifdef IMU_ON

  mpu9250.setFullScaleGyroRange(1); //500 dps
  mpu9250.setFullScaleAccelRange(0); //2G
  mpu9250.setFullScaleMagRange(1); //0.15 mG per LSB, 16 bits +-, so 32767*0.15 = 4915,05 mG

  mpu9250.setXGyroOffset(64);
  mpu9250.setYGyroOffset(-49);
  mpu9250.setZGyroOffset(40);

  //mpu9250.setXAccelOffset(0); //These don't work for now
  //mpu9250.setYAccelOffset(0);
  //mpu9250.setZAccelOffset(0);

  mpu9250.initMPU9250();  //Set scale and offsets before this

  mpu9250.initAK8963(mpu9250.factoryMagCalibration);
  // Initialize device for active mode read of magnetometer
  // Get sensor resolutions, only need to do this once
  mpu9250.getAres();
  mpu9250.getGres();
  mpu9250.getMres();
#endif


#ifdef DEBUGSERIAL_ON
  Serial.begin(115200);  //Debugserial
#endif

#ifdef ROS_MESSAGING_ON
  //Set and allocate length of arrays in those messages that include them
  //  joy_msg.axes_length = 4;
  //  joy_msg.buttons_length = 4;
  //  joy_msg.axes = (float*) malloc(sizeof(float) * 4);
  //  joy_msg.buttons = (int32_t*) malloc(sizeof(int32_t) * 4);
  //  memset(joy_msg.axes,0,4*sizeof(float)); //Initialize this memory to zero
  //  memset(joy_msg.buttons,0,4*sizeof(int32_t));


  nh.initNode();

  nh.advertise(electricsPub);
  nh.advertise(encoderPub);
  nh.advertise(ultrasoundPub);
  nh.advertise(jointPub);
  nh.advertise(imuPub);
  nh.advertise(joyPub);

  nh.subscribe(baseDriveSub);
#endif

  beeper.AddToQueue(Beeperer::StartBeep);

  wdt_reset();
  wdt_enable(WDTO_250MS);
}

void loop() {

  RadioProcess();
#ifdef ROS_MESSAGING_ON
  nh.spinOnce();
#endif
  //MPUHandler();
  DebugSerial();
  BaseDrive();
  AnalogReader();
  //CommStructUpdater();
  BoardLeds();
  SatelliteHandler();
  Beeper();
  //LoopRateChecker();
  MasterPublisher();
  //RadioWaker();

  Cutoff();
  wdt_reset();
}

void Beeper() {
  static unsigned long audioStamp = 0;
  static unsigned long warnStamp = 0;
  //Beep when battery is getting low
  if (millis() - warnStamp > 10000) {
    warnStamp = millis();
    if (batteryVoltage.filteredValue() < BATT_AUDIO_WARN_MV) {
      beeper.AddToQueue(Beeperer::BattLow);
    }
  }
  //Run the beeping function for all beeps
  if (millis() - audioStamp > 10) {
    audioStamp = millis();
    beeper.Run();
  }
}

void MasterPublisher() {
#ifdef ROS_MESSAGING_ON
  static unsigned long stamp = 0;
  static unsigned long stamp2 = 0;
  unsigned long tim = micros();

#ifdef IMU_ON
  if (tim - stamp2 > 9700) { //About 100 hz
    stamp2 = tim;

    imu_msg.header.stamp = nh.now();

    mpu9250.readAccelData(mpu9250.accelCount);  // Read the x/y/z adc values

    imu_msg.ax = mpu9250.accelCount[0]; // - mpu9250.accelBias[0];
    imu_msg.ay = mpu9250.accelCount[1]; // - mpu9250.accelBias[1];
    imu_msg.az = mpu9250.accelCount[2]; // - mpu9250.accelBias[2];

    mpu9250.readGyroData(mpu9250.gyroCount);  // Read the x/y/z adc values

    imu_msg.gx = mpu9250.gyroCount[0];
    imu_msg.gy = mpu9250.gyroCount[1];
    imu_msg.gz = mpu9250.gyroCount[2];

    mpu9250.readMagData(mpu9250.magCount);  // Read the x/y/z adc values

    // Calculate the magnetometer values in milliGauss
    imu_msg.mx = mpu9250.magCount[0];
    imu_msg.my = mpu9250.magCount[1];
    imu_msg.mz = mpu9250.magCount[2];

    imuPub.publish( &imu_msg );
  }
#endif
#ifdef I2C_ON
  if (newSatelliteData) {
    newSatelliteData = false;
    ultrasoundPub.publish(&ultrasound_msg);
  }

#endif
  tim = micros();
  if (tim - stamp > 33100) { //About 30 hz
    stamp = tim;

    electrics_msg.motorCurrent = (motorCurrent.filteredValue()) / 1000;
    electrics_msg.satelliteCurrent = (raspiCurrent.filteredValue()) / 1000;
    electrics_msg.servoCurrent = (servoCurrent.filteredValue()) / 1000;
    electrics_msg.ucCurrent = (ucCurrent.filteredValue()) / 1000;
    electrics_msg.batteryVoltage = (batteryVoltage.filteredValue()) / 1000;


    //Encoders published in the same function that runs PID's


    if (radioActive && !sbus.getFailsafeStatus()) { //If radio is active and not in failsafe mode,send values from it

      joy_msg.ax1 = ((float)constrain(map(sbus.getChannel(1), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      joy_msg.ax2 = ((float)constrain(map(sbus.getChannel(2), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      joy_msg.ax3 = ((float)constrain(map(sbus.getChannel(3), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      joy_msg.ax4 = ((float)constrain(map(sbus.getChannel(4), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      joy_msg.ax5 = ((float)constrain(map(sbus.getChannel(5), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      joy_msg.ax6 = ((float)constrain(map(sbus.getChannel(7), 172, 1811, -1024, 1024), -1024, 1024)) / 1024.0;
      //Deadzones
      if (abs(joy_msg.ax1) < 0.02) joy_msg.ax1 = 0.0; if (abs(joy_msg.ax2) < 0.02) joy_msg.ax2 = 0.0; if (abs(joy_msg.ax3) < 0.02) joy_msg.ax3 = 0.0; if (abs(joy_msg.ax4) < 0.02) joy_msg.ax4 = 0.0;
      if (abs(joy_msg.ax5) < 0.02) joy_msg.ax5 = 0.0; if (abs(joy_msg.ax6) < 0.02) joy_msg.ax6 = 0.0;

      joy_msg.buttons = 0;
      //switch A
      joy_msg.buttons |= ((sbus.getChannel(8) > 1700) ? 1 : 0);
      joy_msg.buttons |= ((sbus.getChannel(8) < 300) ? 1 : 0) << 1;
      //switch B
      joy_msg.buttons |= ((sbus.getChannel(9) > 1700) ? 1 : 0) << 2;
      joy_msg.buttons |= ((sbus.getChannel(9) < 300) ? 1 : 0) << 3;
      //switch C
      joy_msg.buttons |= ((sbus.getChannel(10) > 1700) ? 1 : 0) << 4;
      joy_msg.buttons |= ((sbus.getChannel(10) < 300) ? 1 : 0) << 5;
      //switch D
      joy_msg.buttons |= ((sbus.getChannel(11) > 1700) ? 1 : 0) << 6;
      joy_msg.buttons |= ((sbus.getChannel(11) < 300) ? 1 : 0) << 7;


    }

    else {  //Joystick failsafe
      joy_msg.ax1 = 0.0; joy_msg.ax2 = 0.0; joy_msg.ax3 = 0.0; joy_msg.ax4 = 0.0; joy_msg.ax5 = 0.0; joy_msg.ax6 = 0.0; joy_msg.buttons = 0;
    }

    electricsPub.publish( &electrics_msg );
    joyPub.publish(&joy_msg);
    jointPub.publish(&joint_msg);
  }
#endif
}


void BaseDrive() {                  //Times execution, determines drive mode, runs pids and commands motors + servos
  static unsigned long stamp = 0;
  static unsigned long deadman = 0;

  if (micros() - stamp > testPidRate) {  //PIDs are computed at this rate
    stamp = micros();

    int throttle = 0;  //Calculating speed to encoder pulses relation
    throttle = (radioActive) ? (((float)throttleStick) /  1000.0) * pidEncPerMs : 0.0; //Set to zero if no radio, just in case. Throttlestick goes fom -1024 to 1024

    if (baseDriveCmdReceived) { //Zero deadman timer if message received (for raspi drive)
      baseDriveCmdReceived = false;
      deadman = millis();
    }


    if (/*!radioActive || sbus.getFailsafeStatus()*/millis() - deadman < 500) {  //Using commands from raspi if no radio or radio in failsafe (no transmitter)
      if (millis() - deadman < 500) {
        RaspiDrive(true);
      }
      else {
        RaspiDrive(false);
      }
    }
    else {  //Using commands from radio if it's ok
      AckermanDrive(throttle);
    }

#ifdef ROS_MESSAGING_ON

    encoder_msg.frontLeftVel = mot1PID.input / pidEncPerMs; //Send speeds in m/s
    encoder_msg.frontRightVel = mot2PID.input / pidEncPerMs;
    encoder_msg.rearLeftVel = mot3PID.input / pidEncPerMs;
    encoder_msg.rearRightVel = mot4PID.input / pidEncPerMs;
    encoder_msg.frontLeftAngle = -Command.frontLeftServo; //Flip these to make positive counterclockwise from top
    encoder_msg.frontRightAngle = -Command.frontRightServo;
    encoder_msg.rearLeftAngle = -Command.rearLeftServo;
    encoder_msg.rearRightAngle = -Command.rearRightServo;

    encoderPub.publish(&encoder_msg);

#endif


  }
}

void RaspiDrive(boolean drive) { //Passes actuator commands from above

  if (!drive) {  //Deadman switch, stop robot
    Command.frontLeftServo = Command.frontRightServo = Command.rearLeftServo = Command.rearRightServo = 0.0;
    Command.frontLeftMot = Command.frontRightMot = Command.rearLeftMot = Command.rearRightMot = 0.0;
    mot1PID.Reset((float)GetPos1());
    mot2PID.Reset((float)GetPos2());
    mot3PID.Reset((float)GetPos3());
    mot4PID.Reset((float)GetPos4());
  }
  Servo1Write(Servo1.Degrees(Command.frontLeftServo)); Servo2Write(Servo2.Degrees(Command.frontRightServo));
  Servo3Write(Servo3.Degrees(Command.rearLeftServo)); Servo4Write(Servo4.Degrees(Command.rearRightServo));

  //Convert ms to encoder steps/pid cycle/ms
  Mot1Throttle(mot1PID.Compute(Command.frontLeftMot * pidEncPerMs, GetPos1())); Mot2Throttle(mot2PID.Compute(Command.frontRightMot * pidEncPerMs, GetPos2()));
  Mot3Throttle(mot3PID.Compute(Command.rearLeftMot * pidEncPerMs, GetPos3())); Mot4Throttle(mot4PID.Compute(Command.rearRightMot * pidEncPerMs, GetPos4()));
}

void AckermanDrive(int throttle) { //This calculates wheel ackerman angles according to front and rear bicycle model angles
  float upLeftAngle = 0;
  float upRightAngle = 0;
  float downLeftAngle = 0;
  float downRightAngle = 0;

  float frontAngle = 0;
  float rearAngle = 0;

  float x3 = 0;
  float y3 = 0;
  float k1 = 0;
  float k2 = 0;

  frontAngle = -((float)frontWheelStick) / 12.8;
  rearAngle = -((float)rearWheelStick) / 12.8;

  float b1 = wheelbase / 2; //Constant terms of lines
  float b2 = -wheelbase / 2;
  k1 = tan(TORAD * (frontAngle)); //Kulmakerroin of line
  k2 = tan(TORAD * (rearAngle));

  x3 = (b2 - b1) / (k1 - k2);  //Solving convergence points, x
  y3 = k1 * x3 + b1;            //y

  if (abs(x3) < minPivotDistanceX) { //Turn radius too small to achieve (servos cannot turn enough), clamp radius
    if (x3 < 0) x3 = -minPivotDistanceX;
    else x3 = minPivotDistanceX;
  }

  if ((abs(frontAngle) - abs(rearAngle)) < 0.1 && (frontAngle >= 0.0 == rearAngle >= 0.0)) { //Angles are close together, lines wont converge
    upLeftAngle = -frontAngle; //this should not be so
    upRightAngle = -frontAngle;
    downLeftAngle = -rearAngle;
    downRightAngle = -rearAngle;
  }

  else {
    float tempAngle = TODEG * (-atan((-(track / 2) - x3) / ((wheelbase / 2) - y3)));
    upLeftAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1); //Wheel position - convergence point, right angled triangle

    tempAngle = TODEG * (-atan(((track / 2) - x3) / ((wheelbase / 2) - y3)));
    upRightAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);

    tempAngle = TODEG * (-atan((-(track / 2) - x3) / (-(wheelbase / 2) - y3)));
    downLeftAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);

    tempAngle = TODEG * (-atan(((track / 2) - x3) / (-(wheelbase / 2) - y3)));
    downRightAngle = (tempAngle > 0) ?  90.0 - tempAngle : -90.0 + (tempAngle * -1);
  }

  if (abs(frontWheelStick) > 950 && abs(rearWheelStick) > 950 && ((frontWheelStick > 0) ^ (rearWheelStick > 0))) {

    Servo1Write(Servo1.Degrees(inPlaceTurnAngle)); Servo2Write(Servo2.Degrees(-inPlaceTurnAngle));  //In place turning
    Servo3Write(Servo3.Degrees(-inPlaceTurnAngle)); Servo4Write(Servo4.Degrees(inPlaceTurnAngle));

    if (frontWheelStick > 0) {
      Mot1Throttle(mot1PID.Compute(throttle, GetPos1())); Mot2Throttle(mot2PID.Compute(-throttle, GetPos2()));
      Mot3Throttle(mot3PID.Compute(throttle, GetPos3())); Mot4Throttle(mot4PID.Compute(-throttle, GetPos4()));
    }
    else {
      Mot1Throttle(mot1PID.Compute(-throttle, GetPos1())); Mot2Throttle(mot2PID.Compute(throttle, GetPos2()));
      Mot3Throttle(mot3PID.Compute(-throttle, GetPos3())); Mot4Throttle(mot4PID.Compute(throttle, GetPos4()));
    }
  }
  else {

    Servo1Write(Servo1.Degrees(upLeftAngle)); Servo2Write(Servo2.Degrees(upRightAngle));
    Servo3Write(Servo3.Degrees(downLeftAngle)); Servo4Write(Servo4.Degrees(downRightAngle));
    Mot1Throttle(mot1PID.Compute(throttle, GetPos1())); Mot2Throttle(mot2PID.Compute(throttle, GetPos2()));
    Mot3Throttle(mot3PID.Compute(throttle, GetPos3())); Mot4Throttle(mot4PID.Compute(throttle, GetPos4()));
  }


}

void BoardLeds() {
  static boolean low = false;
  static unsigned long timer = 0;
  static unsigned long warnBlinkTimer = 0;
  unsigned long curTime = millis();
  //===========BATTERY LED============
  //This blue led starts blinking at warn level and blinks faster when approaching cutoff
  if (batteryVoltage.filteredValue() < BATT_WARN_MV) {
    unsigned int blinkTime = map(batteryVoltage.filteredValue(), BATT_CUTOFF_MV, BATT_WARN_MV, 10, 1000); //Time between blinks, low voltage is faster

    if (curTime - warnBlinkTimer > blinkTime) {
      //If battery is in lower 5% range between cutoff and warn, keep led lit!!!
      if (batteryVoltage.filteredValue() < ((float)(BATT_WARN_MV - BATT_CUTOFF_MV)) * 0.05 + BATT_CUTOFF_MV) {
        digitalWrite(BLUELED, LOW);
      }
      else digitalWrite(BLUELED, !digitalRead(BLUELED));
      warnBlinkTimer = curTime;
    }
  }
  else digitalWrite(BLUELED, HIGH);

  //=========RADIO ACTIVITY LED=========

  if (radioActive && !sbus.getFailsafeStatus()) {
    PORTJ &= ~_BV(PJ7);
  }
  else {
    PORTJ |= _BV(PJ7);
  }
}

void Servo1Write(unsigned int pos) {
  OCR3A = pos;
}
void Servo2Write(unsigned int pos) {
  OCR5A = pos;
}
void Servo3Write(unsigned int pos) {
  OCR5B = pos;
}
void Servo4Write(unsigned int pos) {
  OCR5C = pos;
}

float EMA(int sample) {
  const float EMA_a = 0.2;  //EMA alpha, low values make it slower
  static float EMA_S = 0;

  EMA_S = (EMA_a * sample) + ((1 - EMA_a) * EMA_S);
  return EMA_S;
}

//=============ENCODER 1==============
ISR(INT6_vect) { //For interrupt 6, motor 1
  if ((PINE & (1 << PE7)) == ((PINE & (1 << PE6)) << 1)) {
    pos1++;
  }
  else {
    pos1--;
  }
}
ISR(INT7_vect) { //For interrupt 7, motor 1
  if ((PINE & (1 << PE7)) == ((PINE & (1 << PE6)) << 1)) {
    pos1--;
  }
  else {
    pos1++;
  }
}

//===========ENCODER 2===============
ISR(INT3_vect) { //For pin18
  if ((PIND & (1 << PD3)) == ((PIND & (1 << PD2)) << 1)) {
    pos2--;
  }
  else pos2++;
}
ISR(INT2_vect) { //For pin19
  if ((PIND & (1 << PD3)) == ((PIND & (1 << PD2)) << 1)) {
    pos2++;
  }
  else pos2--;
}

//===========ENCODER 4================
ISR(INT5_vect) {
  if ( (PINE & _BV(PE5)) == ((PINJ & _BV(PJ6)) >> 1) ) {
    pos4++;
  }
  else pos4--;
}
ISR(PCINT1_vect) {
  if ( (PINE & _BV(PE5)) == ((PINJ & _BV(PJ6)) >> 1) ) {
    pos4--;
  }
  else pos4++;
}

//==========ENCODER 3=================
ISR(INT4_vect) {
  if ( (PINE & _BV(PE4)) << 3 == (PINK & _BV(PK7)) ) {
    pos3--;
  }
  else pos3++;
}
ISR(PCINT2_vect) {
  if ( (PINE & _BV(PE4)) << 3 == (PINK & _BV(PK7)) ) {
    pos3++;
  }
  else pos3--;
}

long GetPos1() {
  long pul = 0;
  EIMSK &= ~(_BV(INT6) | _BV(INT7));  //Disable the interrupts that increment this encoder counter
  pul = pos1;
  EIMSK |= _BV(INT6) | _BV(INT7);   //Enable them
  return pul;
}

long GetPos2() {
  long pul = 0;
  EIMSK &= ~(_BV(INT2) | _BV(INT3)); //Disable the interrupts that increment this encoder counter
  pul = pos2;
  EIMSK |= _BV(INT2) | _BV(INT3);   //Enable them
  return pul;
}

long GetPos4() {
  long pul = 0;
  EIMSK &= ~_BV(INT5);  //Disable the interrupts that increment this encoder counter
  PCMSK1 &= ~_BV(PCINT15);
  pul = pos4;
  EIMSK |= _BV(INT5); //Enable them
  PCMSK1 |= _BV(PCINT15);
  return pul;
}

long GetPos3() {
  long pul = 0;
  EIMSK &= ~_BV(INT4);  //Disable the interrupts that increment this encoder counter
  PCMSK2 &= ~_BV(PCINT23);
  pul = pos3;
  EIMSK |= _BV(INT4); //Enable them
  PCMSK2 |= _BV(PCINT23);
  return pul;
}

void RadioProcess() {
  static bool radioGoing = false;
  static unsigned long stamp = 0;
  static unsigned long lastToggle = 0;
  static boolean toggleArmed = false;
  unsigned long cur = millis();
  sbus.process(cur);

  if (micros() - stamp > 999) {
    stamp = micros();

    throttleStick = constrain(map(sbus.getChannel(1), 172, 1811, -1024, 1024), -1024, 1024);
    frontWheelStick = constrain(map(sbus.getChannel(2), 172, 1811, -1024, 1024), -1024, 1024);
    rearWheelStick = constrain(map(sbus.getChannel(4), 172, 1811, -1024, 1024), -1024, 1024);
    leftSlider = constrain(map(sbus.getChannel(5), 172, 1811, -1024, 1024), -1024, 1024);
    rightSlider = constrain(map(sbus.getChannel(6), 172, 1811, -1024, 1024), -1024, 1024);

    if (sbus.getChannel(12) > 1300) {  //Toggle led modes
      if (toggleArmed) {
        ledController++;
        beeper.AddToQueue(Beeperer::Bip);
        if (ledController > 4) ledController = 0;
        toggleArmed = false;
      }
    }
    else {
      if (cur - lastToggle > 100) toggleArmed = true;
    }

    radioActive = (cur - sbus.getLastTime() < radioTimeout) && !sbus.getFailsafeStatus();  //If radio is connected and parsed right

    if (!radioActive) {
      
      if(!sbus.getFailsafeStatus()){  //Only try to wake radio if it is disconnected
        RadioWaker();
      }
      
      if (radioGoing) {
        radioGoing = false;
        beeper.AddToQueue(Beeperer::Bop);
      }
    }
    else {
      if (!radioGoing) {
        radioGoing = true;
        beeper.AddToQueue(Beeperer::Bip);
      }
    }
  }
}

void RadioWaker() {
  static unsigned long lastTry = 0;
  if (millis() - lastTry > 100) {
    lastTry = millis();
    Serial3.end();
    sbus.begin(false);
  }
}

void DebugSerial() {
#ifdef DEBUGSERIAL_ON
  static unsigned long stamp = 0;
  if (millis() - stamp > 50) {
    //Serial.println(x3);
    //Serial.println(raspiCurrent.filteredValue());
    //Serial.println(servoCurrent.filteredValue());

    //Serial.println(servoCurrent.filteredValue()+motorCurrent.filteredValue()+ucCurrent.filteredValue());
    //Serial.println(batteryVoltage.filteredValue() / 1000);
    //Serial.print(mpu9250.accelCount[0]); Serial.print('\t');Serial.print(mpu9250.accelCount[1]); Serial.print('\t');Serial.println(mpu9250.accelCount[2]);
    //Serial.print(mpu9250.gyroCount[0]); Serial.print('\t');Serial.print(mpu9250.gyroCount[1]); Serial.print('\t');Serial.println(mpu9250.gyroCount[2]);
    //Serial.print(pitchEstimateGyro); Serial.print('\t');Serial.print(rollEstimateGyro); Serial.print('\t');Serial.println(yawEstimateGyro);
    //Serial.print(servoPos1.filteredValue()); Serial.print('\t'); Serial.print(servoPos2.filteredValue());
    //Serial.print('\t'); Serial.print(servoPos3.filteredValue()); Serial.print('\t'); Serial.println(servoPos4.filteredValue());
    //Serial.print(encPerMeter); Serial.print('\t');Serial.println(pidEncPerMs);
    //Serial.print(frontAngle); Serial.print('\t');Serial.println(rearAngle);
    //Serial.println(ledController);

    //==========SERVOS===============
    //Serial.print(OCR5A); Serial.print('\t'); Serial.print(OCR3A); Serial.print('\t');Serial.print(OCR5B); Serial.print('\t'); Serial.println(OCR5C);

    //===========RADIO==============
    //Serial.print(leftSlider);Serial.print('\t');Serial.println(rightSlider);
    //Serial.print(radioActive);Serial.print('\t'); Serial.print(sbus.getChannel(1));Serial.print('\t');Serial.println(sbus.getChannel(2));
    Serial.print(radioActive); Serial.print('\t'); Serial.print(sbus.getGoodFrames()); Serial.print('\t'); Serial.print(sbus.getLostFrames()); Serial.print('\t'); Serial.println(sbus.getDecoderErrorFrames());

    //=========ACKERMANN============


    //Serial.print(frontAngle);Serial.print('\t');Serial.println(rearAngle);
    //Serial.print(x3);Serial.print('\t');Serial.println(y3);
    //Serial.print(k1);Serial.print('\t');Serial.println(k2);
    //Serial.print(upRightAngle);Serial.print('\t');Serial.print(upLeftAngle);Serial.print('\t');
    //Serial.print(downRightAngle);Serial.print('\t');Serial.println(downLeftAngle);
    //Serial.println(loopRate);
    //Serial.print(sbus.getGoodFrames()); Serial.print('\t');Serial.print(sbus.getLostFrames()); Serial.print('\t');Serial.println(sbus.getDecoderErrorFrames());
    //float angeeli = -TODEG*atan( (/*-(track/2)*/-x3)/((wheelbase/2)-y3));
    //Serial.println((angeeli>0) ?  90-angeeli : -90+(angeeli*-1));//Serial.print('\t');Serial.println(TODEG*atan(-1));

    //==========SONAR=============
    //Serial.print(sonarStruct.frontLeftCm); Serial.print('\t'); Serial.print(sonarStruct.frontCenterCm); Serial.print('\t'); Serial.println(sonarStruct.frontRightCm);
    //Serial.print(sonarStruct.frontCenterCm); Serial.print('\t'); Serial.println(sonarMsgFront.servo);

    //============i2C==================
    //Serial.print(Command.frontLeftMot); Serial.print('\t'); Serial.println(Command.frontLeftServo);
    //Serial.print(ledMsg.mode); Serial.print('\t');  Serial.println(ledMsg.modeData);

    //========ENCODER/PID===============
    //Serial.print(throttleStick / 4); Serial.print('\t'); Serial.println(mot1PID.input);// Serial.print('\t'); Serial.print('\t'); Serial.println(mot1PID.output);
    //Serial.println(encoderSpeed);
    //Serial.print(GetPos1()); Serial.print(','); Serial.print(GetPos2()); Serial.print(','); Serial.print(GetPos3()); Serial.print(','); Serial.println(GetPos4());
    //Serial.print(GetPos1());Serial.print('\t'); Serial.print(-GetPos2());Serial.print('\t'); Serial.print(GetPos3());Serial.print('\t'); Serial.println(-GetPos4());


    //=============ROS==================
    //Serial.print(Command.frontLeftMot); Serial.print('\t'); Serial.println(Command.frontRightMot);

    stamp = millis();
  }
#endif
}

void Mot2Throttle(int thro) { //+-255
  if (thro > 0) { //Going forward
    digitalWrite(MOT2A, HIGH);
    digitalWrite(MOT2B, LOW);

  }
  else { //Going backward
    digitalWrite(MOT2A, LOW);
    digitalWrite(MOT2B, HIGH);
  }
  thro = max(min(thro, 255), -255);
  analogWrite(MOT2PWM, abs(thro));
}
void Mot1Throttle(int thro) { //+-255
  if (thro > 0) { //Going forward
    digitalWrite(MOT1A, LOW);
    PORTH |= _BV(PH7); //MOT1B high
  }
  else { //Going backward
    digitalWrite(MOT1A, HIGH);
    PORTH &= ~_BV(PH7); //MOT1B low
  }
  thro = max(min(thro, 255), -255);
  analogWrite(MOT1PWM, abs(thro));
}
void Mot3Throttle(int thro) { //+-255
  if (thro > 0) { //Going forward
    PORTJ &= ~_BV(PJ4); //MOT3A low
    PORTJ |= _BV(PJ3); //MOT3B high

  }
  else { //Going backward

    PORTJ |= _BV(PJ4); //MOT3A high
    PORTJ &= ~_BV(PJ3); //MOT3B low
  }
  thro = max(min(thro, 255), -255);
  analogWrite(MOT3PWM, abs(thro));
}
void Mot4Throttle(int thro) { //+-255
  if (thro > 0) { //Going forward
    digitalWrite(MOT4A, HIGH);
    digitalWrite(MOT4B, LOW);
  }
  else { //Going backward
    digitalWrite(MOT4A, LOW);
    digitalWrite(MOT4B, HIGH);

  }
  thro = max(min(thro, 255), -255);
  analogWrite(MOT4PWM, abs(thro));
}
unsigned int ADCToMilliamps(unsigned long raw, unsigned int resolution) { //Resolution in volts per ampere for the current sensor (set by resistor)
  unsigned long milliamps = ((raw * sys_5v / 1024) * 10) / resolution;
  return milliamps;
}
unsigned int ADCToMillivolts(unsigned long raw) {
  unsigned long voltDivider = ((raw * BATT_DIV_R1 + raw * BATT_DIV_R2) * 10) / BATT_DIV_R2; //Scale ADC number across voltage divider with 10 multi to keep resolution
  unsigned int volts = (voltDivider * sys_5v) / 1024; //multiply by adc reference volts (~5V, this plus the later 10 make result be in millivolts), divide by ADC resolution
  return volts;
}


void AnalogReader() { //This handles reading of all the analog channels
  //const byte nChannels = sizeof(analogArray)/sizeof(analogArray[0]); //This many channels are in use
  static unsigned long stamppi = 0;
  static byte i = 0;

  if (millis() - stamppi > 5) {
    stamppi = millis();
    //analogArray[i]->value = analogRead(analogArray[i]->pin); //Read current channel

    byte currentPin = analogArray[i]->pin();
    switch (currentPin) { //Process value depending on what it is
      case curMotADC:
        analogArray[i]->value = ADCToMilliamps(analogRead(currentPin), 1);
        analogArray[i]->runEMA();
        break;
      case curRaspADC:
        analogArray[i]->value = ADCToMilliamps(analogRead(currentPin), 1);
        analogArray[i]->runEMA();
        break;
      case battSensADC:
        analogArray[i]->value = ADCToMillivolts(analogRead(currentPin));
        analogArray[i]->runEMA();
        break;
      case curServADC:
        analogArray[i]->value = ADCToMilliamps(analogRead(currentPin), 1);
        analogArray[i]->runEMA();
        break;
      case curUcADC:
        analogArray[i]->value = ADCToMilliamps(analogRead(currentPin), 2);
        analogArray[i]->runEMA();
        break;
      default: //If the channel does not need special processing, just read the raw value
        analogArray[i]->value = analogRead(currentPin);
        analogArray[i]->runEMA();
        break;
    }

    i++;
    if (i > 8) i = 0;
    //if(i > sizeof(analogArray)/sizeof(analogArray[0])); //No worky!
  }
}

void LoopRateChecker() {
  static unsigned long stamppi = 0;
  static unsigned int loops = 0;
  loops++;
  unsigned long current = millis();
  if (current - stamppi > 999) {
    stamppi = current;
    loopRate = loops;
    loops = 0;
  }
}


//void MPUHandler() {
//
//  if (mpu9250.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
//  {
//    mpu9250.readAccelData(mpu9250.accelCount);  // Read the x/y/z adc values
//
//    // Now we'll calculate the accleration value into actual g's
//    // This depends on scale being set
//    mpu9250.ax = (float)mpu9250.accelCount[0] * mpu9250.aRes; // - mpu9250.accelBias[0];
//    mpu9250.ay = (float)mpu9250.accelCount[1] * mpu9250.aRes; // - mpu9250.accelBias[1];
//    mpu9250.az = (float)mpu9250.accelCount[2] * mpu9250.aRes; // - mpu9250.accelBias[2];
//
//    pitchEstimateAccel = atan2(mpu9250.ay , mpu9250.az) * 57.3;
//    rollEstimateAccel = atan2((- mpu9250.ax) , sqrt(mpu9250.ay * mpu9250.ay + mpu9250.az * mpu9250.az)) * 57.3;
//
//    mpu9250.readGyroData(mpu9250.gyroCount);  // Read the x/y/z adc values
//
//    // Calculate the gyro value into actual degrees per second
//    // This depends on scale being set
//    mpu9250.gx = (float)mpu9250.gyroCount[0] * mpu9250.gRes;
//    mpu9250.gy = (float)mpu9250.gyroCount[1] * mpu9250.gRes;
//    mpu9250.gz = (float)mpu9250.gyroCount[2] * mpu9250.gRes;
//
//    static unsigned long loopMicros = 0;
//    float timeFactor = (float)(micros() - loopMicros) / 1000000; //Do this here to avoid duplicating too much
//    pitchEstimateGyro += mpu9250.gx * timeFactor;
//    rollEstimateGyro += mpu9250.gy * timeFactor;
//    yawEstimateGyro += mpu9250.gz * timeFactor;
//    loopMicros = micros();
//
//    mpu9250.readMagData(mpu9250.magCount);  // Read the x/y/z adc values
//
//    // Calculate the magnetometer values in milliGauss
//    // Include factory calibration per data sheet and user environmental
//    // corrections
//    // Get actual magnetometer value, this depends on scale being set
//    mpu9250.mx = (float)mpu9250.magCount[0] * mpu9250.mRes
//                 * mpu9250.factoryMagCalibration[0] - mpu9250.magBias[0];
//    mpu9250.my = (float)mpu9250.magCount[1] * mpu9250.mRes
//                 * mpu9250.factoryMagCalibration[1] - mpu9250.magBias[1];
//    mpu9250.mz = (float)mpu9250.magCount[2] * mpu9250.mRes
//                 * mpu9250.factoryMagCalibration[2] - mpu9250.magBias[2];
//  }
//}

void Cutoff() { //Shuts off the robot if battery voltage is too low
  static unsigned long stamp = 0;
  static byte hits = 0;
  if (millis() - stamp > 100 && millis() > 2000) { //Sample every 100 ms, ignore first 2 secs after boot for filtered reading to stabilize
    if (batteryVoltage.filteredValue() < BATT_CUTOFF_MV) hits++; //A hit is when voltage is lower than cutoff limit, 10 of these are needed to cutoff
    else hits = 0; //Reset counter  in other case
    stamp = millis();
  }
#ifndef CUTOFF_OVERRIDE
  if (hits > 50) PORTJ &= ~_BV(PJ2); //Pull cutoff pin low, turning bot off
#endif
}


void SatelliteHandler() {
#ifdef I2C_ON
  static unsigned long asd = 0;
  if (millis() - asd > 33) {
    asd = millis();
    ledMsg.mode = ledController;
    ledMsg.modeData = LedTester(ledController); //Make message for leds
    ledMsg.servoPos = (uint16_t)(leftSlider + 30000);  //Servo pos for front satellite, slider range +-1024 to uint
    //Serial.println("testing leds, to the ultra I shall send THIS:");
    SendAnything(FRONT_ULTRA, ledMsg);        //After requesting data from the ultras, they run timing critical leds and ping ultrasound.
    GetAnything(FRONT_ULTRA, sonarMsgFront);  //This takes ~30 ms, then it's OK to send stuff to it again (not doing timing critical tasks)
    ledMsg.servoPos = (uint16_t)(rightSlider + 30000);  //Servo pos for front satellite, slider range +-1024 to uint
    SendAnything(REAR_ULTRA, ledMsg);         //So first send info to it (it's waiting), then request (it does it stuff and waits for next msg)
    GetAnything(REAR_ULTRA, sonarMsgRear);    //This means that LED and ultra update rates are defined here, by the rate they are polled

#ifdef ROS_MESSAGING_ON
    ultrasound_msg.frontWhich = sonarMsgFront.which;
    ultrasound_msg.frontCm = sonarMsgFront.meas;
    joint_msg.frontUltraAngle = sonarMsgFront.servo;  //Should map to proper radians here

    ultrasound_msg.rearWhich = sonarMsgRear.which;
    ultrasound_msg.rearCm = sonarMsgRear.meas;
    joint_msg.rearUltraAngle = sonarMsgRear.servo;
#endif

    newSatelliteData = true;

  }
#endif
}

bool IsPositive(float num) {
  if (num >= 0) return true;
  else return false;
}
