#include <Arduino.h>
#include "tweeter.h"


Beeperer::Beeperer(void) {
  _beepables = false;
  _noteNumber = 0;
  _noteCounter = 0;
  _currentIndex = 0;
  for (int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
    _queue[i] = -1; //Initialize all members as unused
  }

}
void Beeperer::Run() {
  if (Beepables()) {
    if (_queue[_currentIndex] >= 0) {
      //Serial.print("playing tune "); Serial.print(_queue[_currentIndex]); Serial.print(" note "); Serial.print(_noteNumber); Serial.print(" counts "); Serial.println(_noteCounter);
      if (_noteNumber < tunelist[_queue[_currentIndex]].noteCount) {

        if (tunelist[_queue[_currentIndex]].notelist[_noteNumber].freq == 0) { //Silent note if frequency is 0
          noToneAC();
        }
        else {                              //Else play the note
          toneAC(tunelist[_queue[_currentIndex]].notelist[_noteNumber].freq);
        }

        if (_noteCounter < tunelist[_queue[_currentIndex]].notelist[_noteNumber].tim) {
          _noteCounter++;
        }
        else {
          _noteCounter = 0;
          _noteNumber++;
        }
      }
      else {        //Reached end of current tune, mark current index as empty and zero counters
        _queue[_currentIndex] = -1;
        _noteCounter = 0;
        _noteNumber = 0;
        noToneAC();
      }
    }
    else {
      //Serial.println("Finding new tune");
      int next = _GetNext();
      if (next < 0) {
        _beepables = false; //Record whether or not there are things in queue
      }
      else {
        _currentIndex = next;
        //Serial.print("Current index now "); Serial.print(_currentIndex); Serial.print(" and has tone "); Serial.println(_queue[_currentIndex]);
        _beepables = true;
      }
    }
  }
}

void Beeperer::AddToQueue(int beepType) {
  if (beepType >= 0 && beepType < (sizeof(tunelist) / sizeof(tunelist[0]))) {  //If beep type is within bounds
//
//    for (int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
//      Serial.print(_queue[i]);
//    }
//    Serial.println();

    for (int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
      //Serial.print(_queue[i]);
      if (_queue[i] < 0) {
        _queue[i] = beepType;                              //Insert into first free slot
        _beepables = true;  //There must now be things in the queue as we just put one in
        //Serial.print("Added tone "); Serial.print(beepType); Serial.print(" to index "); Serial.println(i);
        break;
      }
    }
  }
}

bool Beeperer::Beepables() {
  return _beepables;
}

int32_t Beeperer::_GetNext() {  //Returns index of a valid tune, or negative if there are none
  int32_t returnable = -1;
  for (int i = 0 ; i < (sizeof(_queue) / sizeof(_queue[0])) ; i++) {
    if (_queue[i] >= 0) {
      returnable = i; //return index of first item that is valid (not negative);
//      Serial.print("Found tune "); Serial.print(returnable); Serial.print(" at index "); Serial.println(i);
//
//      for (int j = 0 ; j < (sizeof(_queue) / sizeof(_queue[0])) ; j++) {
//        Serial.print(_queue[j]);
//      }
//      Serial.println();
      break;
    }
  }
  //if (returnable < 0) Serial.println("Found no new tunes");
  return returnable;
}
