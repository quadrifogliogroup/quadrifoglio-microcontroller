#ifndef tweeter_cpp
#define tweeter_cpp

#include <Arduino.h>
#include <toneAC.h>


class Beeperer {
  public:

    enum Tunes
    {
      Tester,
      StartBeep,
      HomeLock,
      GPSLock,
      BattLow,
      BattVeryLow,
      Overspeed,
      OvercurrentMot,
      OvercurrentBat,
      OvertempFet,
      Pushback,
      Critical,
      ReadyMode,
      Bip,
      Bop
    };
    
    Beeperer(void);
    void Run();                       //Beeps
    void AddToQueue(int beepType);    //Tries to add a tune in the queue, does not do so if it is full
    bool Beepables();                 //True if there is something in the queue

  private:
    int32_t _GetNext(); //Returns index to next tune to play from queue, or negative if there is none
    int32_t _queue[5];  //A queue of integers representing tunes in the tune list. Negative numbers means empty
    bool _beepables;     //If there is anything to do
    uint32_t _noteNumber;   //Position in the note field
    uint32_t _noteCounter;   //Time position within a note, in beats (One beat is the time between calls to the Run() method

    uint32_t _queueDelay;  //How many beats of delay between cycles
    int32_t _currentIndex;  //Index of currently playing tune

    struct note {
      uint32_t freq;   //Frequency of note
      uint32_t tim;    //How long the note is, in beats
    };
    struct tune {
      note* notelist;    //Pointer to an array of notes
      uint32_t noteCount;  //The amount of notes in the array
    };

    note tester[11] = { {3200, 2} , {0, 1} , {3000, 2}, {0, 1} , {2800, 2}, {0, 1}, {3200, 2} , {0, 1} , {3000, 2}, {0, 1} , {2800, 4}};
    note startbeep[5] = { {2400, 1} , {0, 1} , {2000, 1}, {0, 1} , {2800, 4}};
    note homelock[7] = { {3200, 1} , {0, 1} , {3200, 1}, {0, 1} , {3200, 1}, {0, 1} , {3700, 1}};
    note gpslock[3] = { {3200, 2} , {0, 10} , {3200, 2}};
    note battlow[5] = { {3200, 2} , {0, 2} , {3000, 2}, {0, 2} , {2800, 4}};
    note battverylow[12] = { {3200, 2} , {0, 1} , {3000, 2}, {0, 1} , {2800, 2}, {0, 1}, {2600, 2} , {0, 1} , {2400, 2}, {0, 1} , {2200, 4}, {0, 8}};
    note overspeed[6] = { {3200, 2} , {0, 2} , {3200, 2}, {0, 2} , {3200, 2}, {0, 8}};
    note overcurmot[4] = { {3200, 2} , {0, 2} , {2800, 4}, {0, 8}};
    note overcurbat[6] = { {3200, 2} , {0, 2} , {2800, 2}, {0, 2} , {2800, 2}, {0, 8}};
    note overtempfet[6] = { {2800, 2} , {0, 2} , {3200, 4}, {0, 2} , {2800, 2}, {0, 8}};
    note pushback[7] = { {2800, 2} , {0, 2} , {3000, 4}, {0, 2} , {3200, 2}, {0, 2} , {3400, 2}};
    note critical[1] = { {3200, 6}};
    note readymode[5] = { {2400, 1} , {0, 1} , {2400, 1}, {0, 1} , {3400, 6}};
    note bip[1] = { {3800, 1}};
    note bop[1] = { {2400, 1}};

    tune tunelist[15] = {
      {tester, 11},
      {startbeep, 5},
      {homelock, 7},
      {gpslock, 3},
      {battlow, 5},
      {battverylow, 12},
      {overspeed,6},
      {overcurmot,4},
      {overcurbat,6},
      {overtempfet,6},
      {pushback,7},
      {critical,1},
      {readymode,5},
      {bip,1},
      {bop,1}
    };

};


#endif
